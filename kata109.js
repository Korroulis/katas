// 1. Chunk array 

function chunk (arr,n) {
    var result = [];
    if (n>arr.length) {
        return arr;
    }
    for (var i=0,len=arr.length; i<len; i+=n)
      result.push(arr.slice(i,i+n));
    return result;
}

// 2. Pad with x

function pad(str,str2,n) {
    var result = str;
    if (n<=str.length) {
        return str;
    }
    for (i=0;i<n-str.length;i++) 
        result=result+str2;
    return result;
}

// 3. Flatten the array

function flatten (arr) {
    arr = arr.reduce((a, b) => a.concat(b), []);
    return arr;
}

// 4. Reverse


function reverseArr(n) {
   return n.toString(10).replace(/\D/g, '0').split('').map(Number).reverse();
};