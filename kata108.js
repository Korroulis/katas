//1. Most wanted letter

function mostWanted(str) {

    const regex = /[a-z]/g;
    let myArray = str.toLowerCase().match(regex).sort();
    return myArray.reduce(
        (a,b,i,arr)=>
         (arr.filter(v=>v===a).length>=arr.filter(v=>v===b).length?a:b),
        null)
}

//2. Long words

function longWords(str1) {
    let count=0;
    let curr_count=1;
    for (i=0; i<str1.length-1; i++) {
        if (str1[i+1]===str1[i]) {
            curr_count ++
        }
        else { 
            if (curr_count > count) 
            { 
                count = curr_count; 
            } 
            curr_count = 1; 
        } 

    }
    return curr_count;
}

//3. Fibonacci

function fibo(num) {
    let arr_fibo=[0,1];
    if (num == 1) {
        return [0];
    }
    else if (num == 2) {
        return arr_fibo;
    }
    for (i=1;i<num-1;i++) {
        return arr_fibo.push(arr_fibo[i-1]+arr_fibo[i]);
    }
}

//4. fizzBuzz

function fizzBuzz(num) {
    let arr_fiz=[];
    arr_fiz.length=num;
    for (i=0; i<num.length-1; i++) {
        if (i%15===0) {
            arr_fiz.push('FizzBuzz');
        }
        else if (i%3===0) {
            arr_fiz.push('Fizz');
        }
        else if (i%5===0) {
            arr_fiz.push('Buzz');
        }
        arr_fiz.push(i+1);
    }  
    console.log(arr_fiz); 
}