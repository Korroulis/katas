/////////////////////////////////

function LargerThan (arr,max) {
    let arrTotal = arr.reduce(function(sum, value) {
        return sum + value;
    }, 0);
    return arrTotal > max ? true : false;
}

/////////////////////////////////////

function addLegs (n_chick,n_cow,n_pig) {
    return sum_legs=2*n_chick+4*n_cow+4*n_pig;
}

//////////////////////

function wordLengths (arr) {
    return arr.map(function (str) {;
        return str.length;
      });
}

//////////////////////

var romanMatrix = [
    [1000, 'M'],
    [900, 'CM'],
    [500, 'D'],
    [400, 'CD'],
    [100, 'C'],
    [90, 'XC'],
    [50, 'L'],
    [40, 'XL'],
    [10, 'X'],
    [9, 'IX'],
    [5, 'V'],
    [4, 'IV'],
    [1, 'I']
];

function romanNumerals(num) {
    if (num === 0) {
      return '';
    }
    for (var i = 0; i < romanMatrix.length; i++) {
      if (num >= romanMatrix[i][0]) {
        return romanMatrix[i][1] + romanNumerals(num - romanMatrix[i][0]);
      }
    }
}

var romanMatrix2 = [
    [1000, 'M'],
    [500, 'D'],
    [100, 'C'],
    [50, 'L'],
    [10, 'X'],
    [5, 'V'],
    [1, 'I']
];

function romanNumerals2(num) {
    if (num === 0) {
      return '';
    }
    for (var i = 1; i < romanMatrix2.length-1; i++) {
        if (num >= 1000) {
            return romanMatrix2[0][1] + romanNumerals2(num - romanMatrix2[0][0]);
            }
        else { 
            if (num >= romanMatrix2[i][0] && i%2===0 && num/romanMatrix2[i][0]===4) {
                return romanMatrix2[i][1] + romanMatrix2[i-1][1] + romanNumerals2(num - 4*romanMatrix2[i][0]);
            }
            else if (num >= romanMatrix2[i][0] && i%2!==0 && (num-romanMatrix2[i][0])*10/romanMatrix2[i-1][0]===4) {
                return romanMatrix2[i+1][1] + romanMatrix2[i-1][1] + romanNumerals2(num - 9*romanMatrix2[i+1][0]);
            }
            else  {
                return romanMatrix2[i][1] + romanNumerals2(num - romanMatrix2[i][0]);
            }
        }   
    }  
}
