// Jackpot true even if the array is empty

function jackpot(arr) {
    return arr.every(val => val === arr[0]);
};

// At least returns an error if array is empty :P

function jackpot_update (arr) {
    return arr.reduce((a, b) => a===b ?  a : !b)===arr[0];
}

// Match the last item

function matchLast(arr) {
    return arr.map((x) =>String(x)).splice(0,arr.length-1).join("")===arr.pop() ? true : false;
    //return arr.splice(0,arr.length-1).join(',');
}

// Anagram

function testAnagram(str1, str2) {
    return str1.toLowerCase().split("").sort().join("") === str2.toLowerCase().split("").sort().join("") ? true : false;
}
