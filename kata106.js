// Filter out numbers multiple of

function filterMultiples(array,n) {
    return array.filter (word=> word % n === 0);
}

// Matcher

function matcher(arr1, arr2) {
    var t;
    if (arr2.length > arr1.length) t = arr2, arr2 = arr1, arr1 = t; // indexOf to loop over shorter
    return arr1.filter(function (e) {
        return arr2.indexOf(e) > -1;
    });
}

// Email to name function

function emailToName (arr) {

    return arr.map(function (str) {
        let index=str.indexOf('@');
        return str.slice(0, index).replace("-"," ").replace("."," ");
      });

}

function matcher2 (sourceA, sourceB) {
    const newArray = [];
    const longest = Math.max(sourceA.length, sourceB.length);
    for (let i = 0; i < longest; i++) {
        if (sourceB.includes(sourceA[i])) {
            newArray.push(sourceA[i]);
        }
    }
    return newArray;
};
