// 1. Odd up even down

function evenOddTransfrom (arr,num) {
    return arr.map((x) => x%2 === 0  ?  x-2*num : x+2*num);
}

// 2. Return the sum of the two smallest numbers

function sumTwoSmallestNums (arr) {
    arr_sortedpos = arr.filter( v => v > 0).sort((a, b) => b - a);
    return arr_sortedpos.slice(Math.max(arr_sortedpos.length - 2, 0)).reduce((a, b) => a+b);
}

// 3. H4ck3r Sp34k

function hackerSpeak (str) {
    var chars = {'a':'4','e':'3','i':'1','o':'0','s':'5'};
    return str.replace(/[aeios]/g, x => chars[x]);
}